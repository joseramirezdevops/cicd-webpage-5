from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello1():
    return 'Hello1 from /'

@app.route('/api')
def hello2():
    return '{"id":0,"message":"Flask: Hello World from Docker api"}'

if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0')